# one more refactor
#
# separate the algorithms to apply from the Signer itself
#
class Signer
  attr_accessor :consumer_id, :private_key, :headers, :group
  EXEC_VERSION = {
    bronze: BronzeSignature,
    silver: SilverSignature,
    gold:   GoldSignature
  }.freeze

  def initialize(channel)
    @consumer_id = channel.consumer_id
    @private_key = channel.private_key
    @group       = channel.group
  end

  def call(http_method:, path:)
    EXEC_VERSION[group].run # ...
  end
end

class BronzeSignature
  def self.run
    # ...
  end
end
class SilverSignature
  def self.run
    # ...
  end
end
class GoldSignature
  def self.run
    # ...
  end
end

########################################################################

class Request
  attr_reader :signer

  def initialize(signer)
    @signer = signer
  end

  def make_request(method:, endpoint:, body: nil, mime: nil)
    signed_header = signer.call(http_method: method, path: endpoint) # seam

    request_object = build_request_object(
      method: method,
      url: endpoint,
      timestamp: signed_header.timestamp,
      signature: signed_header.signature,
      mime: mime,
      body: body
    )

    parse_response(request_object.execute)
  rescue RestClient::Exception => e
    # ...
  end

  def refresh_stats
    make_request(method: :get, endpoint: refresh_stats_url)
  end

  def new_stats
    make_request(method: :get, endpoint: new_stats_url)
  end
end

########################################################################

class Channel < ActiveRecord::Base
  belongs_to :user
  delegate :gold?,   to: :user
  delegate :silver?, to: :user
  delegate :bronze?, to: :user
  delegate :group,   to: :user

  def api
    @api ||= Request.new(signer)
  end

  private

  def signer
    Signer.new(self)
  end
end

########################################################################

class RefreshStats < BaseWorker
  def perform(id)
    api = Channel.find(id).api
    api.refresh_stats
    api.new_stats
  end
end
